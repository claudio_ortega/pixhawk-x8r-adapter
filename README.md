### Pixhawk Telemetry to FrySky X8R Smart Port adaptor unit ###

## Usefulness ##
In order to send telemetry information from a Pixhawk into a Taranis Open TX transmitter/receiver
it is necessary to perform an electrical level conversion, a logical signal invertion,
and finally a signal split. 
This last operation is also needed as the Pixhawk telemetry port features two electrical 
unidirectional lines: one TX and one RX, 
while the Smart Port in the X8R side has only one bidirectional line for both functions.

## Solution ##
This project contains all the information needed to assemble this adapter,
including the schematics and the gerber files needed for PCB manufacturing.

## License ##
This project is distributed with the hope that it might be useful for you,
but is offered WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Beyond the legalese, all this means is that this device works,
and you may also use it, but only at your own risk.
I expect nothing in return, except that if you find this useful, 
then you will give me the due credit in the form of the link below

	https://bitbucket.org/claudio_ortega/pixhawk-x8r-adapter
